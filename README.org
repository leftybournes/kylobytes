* kylobytes

Welcome to my personal journal. This is where I write and share my notes,
thoughts, gripes and ramblings on tech and open source software.

These notes, and this README 😉, are written with [[https://www.gnu.org/software/emacs][Emacs]] and emacs [[https://orgmode.org][org-mode]].
